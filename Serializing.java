// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ClassNotFoundException;

class Serializing {
	public static void main(String[] args) {
		Login loginWrite = new Login("anon", "anon@foo.bar", "qwerty");
		Login loginRead = null;

		try {
			String filename = "login.ser";
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(loginWrite);
			oos.close();
			fos.close();

			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);
			loginRead = (Login) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException e) {
			System.out.println("There was a problem with I/O.");
			System.exit(-1);
		} catch (ClassNotFoundException e) {} // the class is below

		System.out.println("loginWrite = " + loginWrite);
		System.out.println("loginRead  = " + loginRead);
	}
}

class Login implements java.io.Serializable {
	public String username;
	private String email;
	private transient String password; // transient - not serialized

	public Login(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public String toString() {
		return "Login{username:" + username
		       + ", email:" + email
		       + ", password:" + password + "}";
	}
}
