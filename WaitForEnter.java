// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.util.Scanner;

public class WaitForEnter {
	public static void main(String[] args) {
		System.out.println("Press Enter to continue.");
		waitForEnter();
	}

	private static void waitForEnter() {
		Scanner s = new Scanner(System.in);
		s.nextLine();
	}
}
