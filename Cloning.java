// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

class Cloning {
	public static void main(String[] args) throws CloneNotSupportedException {
		testCloneDefault();
		System.out.println("--");
		testCloneCustom();
	}

	private static void testCloneDefault() throws CloneNotSupportedException {
		SheepCloneDefault molly = new SheepCloneDefault("Molly", 1);
		SheepCloneDefault dolly = (SheepCloneDefault) molly.clone();
		// members with primitive types are cloned but
		// references still point to the same objects
		dolly.setAge(6); // won't modify 'molly'
		dolly.getInfo().setName("Dolly"); // but this will
		System.out.println("molly = " + molly); // no longer called "Molly"
		System.out.println("dolly = " + dolly);
	}

	private static void testCloneCustom() throws CloneNotSupportedException {
		SheepCloneCustom molly = new SheepCloneCustom("Molly", 1);
		SheepCloneCustom dolly = (SheepCloneCustom) molly.clone();
		dolly.setAge(6);
		dolly.getInfo().setName("Dolly");
		System.out.println("molly = " + molly); // still "Molly"
		System.out.println("dolly = " + dolly);
	}
}

class SheepInfo implements Cloneable {
	private final String codeName = "6LL3";
	private String name = "";

	public SheepInfo(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return "SheepInfo{codeName:" + codeName +", name:" + name + "}";
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}

abstract class Sheep {
	private SheepInfo info;
	private int age;

	public Sheep(String name, int age) {
		this.info = new SheepInfo(name);
		this.age = age;
	}

	public SheepInfo getInfo() {
		return this.info;
	}

	public void setInfo(SheepInfo info) {
		this.info = new SheepInfo(info.getName());
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String toString() {
		return "Sheep{info:" + info + ", age:" + age + "}";
	}
}

class SheepCloneDefault extends Sheep implements Cloneable {
	public SheepCloneDefault(String name, int age) {
		super(name, age);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}

class SheepCloneCustom extends Sheep implements Cloneable {
	public SheepCloneCustom(String name, int age) {
		super(name, age);
	}

	public void setInfo(SheepInfo info) {
		super.setInfo(info);
	}

	public SheepInfo getInfo() {
		return super.getInfo();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		SheepCloneCustom cloned = (SheepCloneCustom) super.clone();
		cloned.setInfo((SheepInfo) cloned.getInfo().clone());
		return cloned;
	}
}
