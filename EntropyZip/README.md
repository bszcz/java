Entropy Zip
===========

Basic entropy compression program.

Individual file history
-----------------------

This code was merged from `entropy-zip` repository.
Use `git log --follow` for individual file history.
