// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.ezip;

import java.io.FileInputStream;
import java.io.IOException;

public class EZip {
	public static void main(String[] args) {
		String inputFilename = "LICENSE";
		FileInputStream fis = null;
		HuffTree ht = null;
		try {
			fis = new FileInputStream(inputFilename);
			ht = new HuffTree(fis);
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
