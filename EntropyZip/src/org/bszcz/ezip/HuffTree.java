// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.ezip;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

class HuffTree {
	public static final int BYTES = 256;
	private ArrayList<HuffNode> leaves = new ArrayList<HuffNode>(BYTES); // [0, 255] possible byte values
	private int uncompressedSize = 0;
	private int   compressedSize = 0;

	public HuffTree(final FileInputStream fis) throws IOException {
		for (int b = 0; b < BYTES; b++) {
			leaves.add(new HuffNode(0, b));
		}

		while (true) {
			int b = fis.read();
			if (b == -1) {
				break;
			}
			leaves.get(b).count++;
			uncompressedSize += 8; // 8 bits/byte
		}

		for (int b = 0; b < leaves.size(); b++) {
			if (leaves.get(b).count == 0) {
				leaves.remove(b);
				b--;
			}
		}

		grow();
		print(leaves.get(0), 0);
		System.out.println("uncompressed = " + uncompressedSize + " bits");
		System.out.println("  compressed = " +   compressedSize + " bits (without tree)");
	}

	private void grow() {
		while (leaves.size() > 1) {
			Collections.sort(leaves);
			int size = leaves.size();
			int count = leaves.get(size - 1).count
			            + leaves.get(size - 2).count;
			HuffNode hn = new HuffNode(count);
			hn.left  = leaves.get(size - 1);
			hn.right = leaves.get(size - 2);
			leaves.remove(size - 1);
			leaves.remove(size - 2);
			leaves.add(hn);
		}
	}

	private void print(HuffNode hn, int indent) {
		if (hn != null) {
			if (hn.value != HuffNode.EMPTY) {
				compressedSize += indent * hn.count;
			}
			for (int i = 0; i < indent; i++) {
				System.out.print("-");
			}
			System.out.printf("(%d, %d)\n", hn.count, hn.value);
			print(hn.left , indent + 1);
			print(hn.right, indent + 1);
		}
	}
}
