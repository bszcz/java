// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.ezip;

class HuffNode implements Comparable<HuffNode> {
	public static final int EMPTY = -1; // value can be [0, 255] only

	public HuffNode right = null;
	public HuffNode left  = null;

	public int count;
	public int value;

	public HuffNode() {
		this(0, EMPTY);
	}

	public HuffNode(final int count) {
		this(count, EMPTY);
	}

	public HuffNode(final int count, final int value) {
		this.count = count;
		this.value = value;
	}

	@Override
	public int compareTo(HuffNode hn) { // descending by count
		if (this.count < hn.count) {
			return +1;
		} else if (this.count == hn.count) {
			return 0;
		} else {
			return -1;
		}
	}
}
