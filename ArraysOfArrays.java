// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.util.Arrays;

class ArraysOfArrays {
	public static void main(String[] args) {
		final int depth = 5;
		PascalTriangle pt = new PascalTriangle(depth);
		System.out.println(pt);
	}
}

class PascalTriangle {
	private final int[][] rows;

	public PascalTriangle(final int depth) {
		rows = new int[depth][];
		for (int i = 0; i < depth; i++) {
			rows[i] = new int[i + 1];
		}
		fill();
	}

	private void fill() {
		for (int i = 0; i < rows.length; i++) {
			int len = rows[i].length;
			rows[i][0] = 1;
			for (int j = 1; j < len - 1; j++) {
				rows[i][j] = rows[i - 1][j - 1] + rows[i - 1][j];
			}
			rows[i][len - 1] = 1;
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < rows.length; i++) {
			sb.append(Arrays.toString(rows[i]) + "\n");
		}
		return sb.toString();
	}
}
