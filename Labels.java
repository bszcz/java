// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

public class Labels {
	private static final int MAX = 4;

	public static void main(String[] args) {
		System.out.println("\nbreak:");
		forBreak();
		System.out.println("\nlabel:");
		forLabel();
	}

	private static void forBreak() {
		for (int i = 0; i < MAX; i++) {
			for (int j = 0; j < MAX; j++) {
				System.out.println("i = " + i + ", j = " + j);
				if (2 == i && 2 == j) {
					System.out.println("breaking out");
					break;
				}
			}
		}
		System.out.println("out of loops");
	}

	private static void forLabel() {
		outOfLoops:
		for (int i = 0; i < MAX; i++) {
			for (int j = 0; j < MAX; j++) {
				System.out.println("i = " + i + ", j = " + j);
				if (2 == i && 2 == j) {
					System.out.println("breaking out");
					break outOfLoops;
				}
			}
		}
		System.out.println("out of loops");
	}
}
