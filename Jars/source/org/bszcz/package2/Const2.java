// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.package2;

class Const2 {
	static final int c2 = 2000;
}
