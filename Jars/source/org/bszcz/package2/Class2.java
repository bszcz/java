// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.package2;

public class Class2 {
	public static int getConst() {
		return Const2.c2;
	}
}
