// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.package1;

public class Class1 {
	public static int getConst() {
		return Const1.c1;
	}
}
