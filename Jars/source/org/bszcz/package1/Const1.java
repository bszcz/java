// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.package1;

class Const1 {
	static final int c1 = 1000;
}
