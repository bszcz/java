// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package org.bszcz.test;

import org.bszcz.package1.Class1;
import org.bszcz.package2.Class2;

public class Test {
	public static void main(String[] args) {
		System.out.println("Class1.getConst() = " + Class1.getConst());
		System.out.println("Class2.getConst() = " + Class2.getConst());
	}
}
