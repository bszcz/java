// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.util.concurrent.TimeUnit;

class StartAndJoin {
	public static void main(String[] args) {
		final int maxDelay = 5; // seconds
		final int numThreads = 100;

		Thread[] threads = new Thread[numThreads];
		for (int i = 0; i < numThreads; i++) {
			int d = i % (maxDelay + 1);
			threads[i] = new Thread(new SmallDelay(d));
		}

		long t = System.nanoTime();
		for (int i = 0; i < numThreads; i++) {
			threads[i].start();
		}
		System.out.println(numThreads + " threads started");
		try {
			for (int i = 0; i < numThreads; i++) {
				threads[i].join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(numThreads + " threads joined");
		t = System.nanoTime() - t;

		System.out.println("running time was " + (double)t / 1_000_000_000.0 + " seconds");
	}
}

class SmallDelay implements Runnable {
	private final int delay;

	public SmallDelay(final int delay) {
		this.delay = delay;
	}

	public void run() {
		try {
			TimeUnit.SECONDS.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
