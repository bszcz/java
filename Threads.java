// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

class Threads {
	public static void main(String[] args) {
		int numThreads = 4;
		if (args.length > 0) {
			try {
				numThreads = Integer.parseInt(args[0]);
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			}
		}

		System.out.println("Hello Threads !!!");

		HiThread[] threads = new HiThread[numThreads];
		for (int t = 0; t < numThreads; t++) {
			threads[t] = new HiThread(t);
			threads[t].start();
		}

	}
}

class HiThread extends Thread {
	int rank;

	public HiThread(int r) {
		rank = r;
	}

	public void run() {
		System.out.println("Hi from thread number " + rank + " !!!");
	}
}
