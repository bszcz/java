// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// background story: http://youtu.be/NredDjgrwYs

class WaitAndNotify {
	public static void main(String[] args) {
		IMouse imouse = new IMouse();
		imouse.start();
		new LMouse(imouse).start();
	}
}

class IMouse extends Thread { // Industrious Mouse
	private static final int MAX_BERRIES = 16;
	private int berries;

	public IMouse() {
		berries = 0;
	}

	public void run() {
		while (true) {
			findBerry();
		}
	}

	private synchronized void findBerry() {
		while (berries >= MAX_BERRIES) {
			try {
				wait();
			} catch (InterruptedException e) {}
		}
		berries++;
		System.out.println("Industrious Mouse: Found a berry.");
		notifyAll();
	}

	public synchronized void eatBerry() {
		notifyAll(); // could get stuck here without it
		while (berries <= 0) {
			try {
				wait();
			} catch (InterruptedException e) {}
		}
		berries--;
	}
}

class LMouse extends Thread { // Lazy Mouse
	private IMouse imouse;

	public LMouse(IMouse imouse) {
		this.imouse = imouse;
	}

	public void run() {
		while (true) {
			imouse.eatBerry();
			System.out.println("Lazy Mouse: Ate a berry.");
		}
	}
}
