// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import static java.lang.Math.*; // no need for 'Math.', see below

class ImportStatic {
	public static void main(String[] args) {
		double x = sin(sqrt(E + PI)); // instead of 'Math.sin(Math.sqrt(Math.E + Math.PI))'
		System.out.println("x = " + x);
	}
}
