// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

/*
   HORSE
+ SADDLE
--------
  GALLOP

find 1:1 correspondence between:
{ A, D, E, G, H, L, O, P, R, S }
and
{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }
*/

import java.util.Arrays;

class HorseSaddleGallop {
	public static void main(String[] args) {
		PermuteDigits per = new PermuteDigits();
		do {
			if (isAnswer(per)) {
				break;
			}
		} while (per.permute());
	}

	private static final int A = 0;
	private static final int D = 1;
	private static final int E = 2;
	private static final int G = 3;
	private static final int H = 4;
	private static final int L = 5;
	private static final int O = 6;
	private static final int P = 7;
	private static final int R = 8;
	private static final int S = 9;

	private static boolean isAnswer(PermuteDigits per) {
		int[] d = per.getDigits();
		if (d[H] == 0 ||
		    d[S] == 0 ||
		    d[G] == 0) {
			return false;
		}

		int[] horseDigits  = {d[H], d[O], d[R], d[S], d[E]};
		int[] saddleDigits = {d[S], d[A], d[D], d[D], d[L], d[E]};
		int[] gallopDigits = {d[G], d[A], d[L], d[L], d[O], d[P]};

		int horse  = toInt(horseDigits);
		int saddle = toInt(saddleDigits);
		int gallop = toInt(gallopDigits);

		if (horse + saddle == gallop) {
			System.out.println("[A, D, E, G, H, L, O, P, R, S]");
			System.out.println(per);
			System.out.println();
			System.out.println("   HORSE       " + horse);
			System.out.println("+ SADDLE    + " + saddle);
			System.out.println("--------    --------");
			System.out.println("  GALLOP      " + gallop);
			return true;
		} else {
			return false;
		}
	}

	private static int toInt(int[] d) {
		int num = 0;
		int mul = 1;
		for (int i = d.length - 1; i >= 0; i--) {
			num += d[i] * mul;
			mul *= 10;
		}
		return num;
	}
}

class PermuteDigits {
	private final int[] digits = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	private int index1;
	private int index2;

	public PermuteDigits() {
	}

	public int[] getDigits() {
		return digits.clone();
	}

	public boolean permute() {
		if (findIndex1()) {
			findIndex2();
			swap();
			reverse();
			return true;
		} else {
			return false;
		}
	}

	private boolean findIndex1() {
		for (int i = digits.length - 2; i >= 0; i--) {
			if (digits[i] < digits[i + 1]) {
				index1 = i;
				return true;
			}
		}
		return false;
	}

	private void findIndex2() {
		index2 = digits.length - 1;
		while (digits[index1] >= digits[index2]) {
			index2--;
		}
	}

	private void swap() {
		int tmp = digits[index1];
		digits[index1] = digits[index2];
		digits[index2] = tmp;
	}

	private void reverse() {
		index1++;
		index2 = digits.length - 1;
		while (index1 < index2) {
			swap();
			index1++;
			index2--;
		}
	}

	public String toString() {
		return Arrays.toString(digits);
	}
}
