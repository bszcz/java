echo "starting rmiregistry..."
rmiregistry &
rmi_pid=$!
sleep 3

echo "starting server..."
java Server &
srv_pid=$!
sleep 3

echo "starting clients..."
for i in $(seq 1 5); do
	java Client
	sleep 1
done

echo "cleaning up..."
kill -9 $srv_pid
kill -9 $rmi_pid
