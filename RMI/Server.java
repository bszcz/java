// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.rmi.*;

class Server {
	public static void main(String[] args) {
		try {
			Naming.bind("rc", new RemoteCounter());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
