// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.rmi.*;

class Client {
	public static void main(String[] args) {
		final String url = "rmi://localhost/rc";
		try {
			RemoteCounterInterface rci = (RemoteCounterInterface) Naming.lookup(url);
			System.out.println("--");
			System.out.println("rci.get() = " + rci.get());
			System.out.println("rci.increase()");
			rci.increase();
			System.out.println("rci.get() = " + rci.get());
			System.out.println("--");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
