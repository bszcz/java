// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.rmi.*;
import java.rmi.server.*;

public interface RemoteCounterInterface extends Remote {
	public void increase() throws RemoteException;
	public int get() throws RemoteException;
}
