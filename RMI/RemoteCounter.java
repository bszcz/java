// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.rmi.*;
import java.rmi.server.*;

public class RemoteCounter extends UnicastRemoteObject implements RemoteCounterInterface {
	private int counter = 0;

	public RemoteCounter() throws RemoteException {
	}

	public void increase() throws RemoteException {
		counter++;
	}

	public int get() throws RemoteException {
		return counter;
	}
}
