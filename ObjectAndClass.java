// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.io.PrintStream;

class ObjectAndClass {
	public static void main(String[] args) {
		PrintStream out = System.out;

		Object obj = new Object();
		out.println("obj.hashCode() = " + obj.hashCode());
		out.println("obj.toString() = " + obj.toString());
		out.println("obj = " + obj); // calls toString()

		Class cla = obj.getClass();
		out.println("cla.getName() = " + cla.getName());
		out.println("cla = " + cla); // toString() adds 'class' or 'interface' in front
		out.println("cla.isInterface() = " + cla.isInterface());

		Class sup = cla.getSuperclass(); // = null, Object is the root of class hierarchy
		out.println("sup = " + sup);

		out.println("obj.equals(obj) = " + obj.equals(obj));
		out.println("obj.equals(out) = " + obj.equals(out));

		out.println("obj instanceof Object = " + (obj instanceof Object));
		out.println("out instanceof Object = " + (out instanceof Object));
		out.println("obj instanceof PrintStream = " + (obj instanceof PrintStream));
		out.println("out instanceof PrintStream = " + (out instanceof PrintStream));

		listInterfaces(obj);
		listInterfaces(out);
		listInterfaces(cla);

		listConstructors(out);
		listMethods(out);

		// call to finalize() is NOT guaranteed
		// the call can be forced by executing:
		System.runFinalizersOnExit(true);
		// but this method has been deprecated
		// general rule is: "avoid finalizers"
		cryForHelp();
	}

	private static void listInterfaces(Object obj) {
		String name = obj.getClass().getName();
		Class[] ifs = obj.getClass().getInterfaces();
		if (ifs.length == 0) {
			System.out.println("Class " + name + " implements no interfaces.");
		} else {
			System.out.println("Class " + name + " implements these interfaces:");
			for (int i = 0; i < ifs.length; i++) {
				System.out.println("ifs[" + i + "] = " + ifs[i].getName());
			}
		}
	}

	private static void listConstructors(Object obj) {
		String name = obj.getClass().getName();
		Constructor[] cons = obj.getClass().getConstructors();
		System.out.println("Class " + name + " has these constructors:");
		for (int i = 0; i < cons.length; i++) {
			System.out.print("cons[" + i + "] = " + cons[i].getName() + "(");
			Class[] pars = cons[i].getParameterTypes();
			printParameters(pars);
			System.out.println(")");
		}
	}

	private static void listMethods(Object obj) {
		String name = obj.getClass().getName();
		Method[] meths = obj.getClass().getMethods();
		System.out.println("Class " + name + " has these methods (first 10 listed):");
		for (int i = 0; i < meths.length; i++) {
			System.out.print("meths[" + i + "] = ");
			System.out.print(meths[i].getReturnType().getName());
			System.out.print(" " + meths[i].getName() + "(");
			Class[] pars = meths[i].getParameterTypes();
			printParameters(pars);
			System.out.println(")");
			if (i >= 9) {
				break;
			}
		}
	}

	private static void printParameters(Class[] pars) {
		for (int i = 0; i < pars.length; i++) {
			String typeName;
			if (pars[i].isArray()) {
				typeName = pars[i].getComponentType().getName() + "[]";
			} else {
				typeName = pars[i].getName();
			}
			System.out.print(typeName);
			if (i != pars.length - 1) {
				System.out.print(", ");
			}
		}
	}

	private static void cryForHelp() {
		CryForHelp cfh = new CryForHelp();
		System.out.println("cfh = " + cfh);
	}
}

class CryForHelp extends Object {
	@Override
	protected void finalize() throws Throwable {
		System.err.println("Help! The Garbage Collector is here!");
		super.finalize(); // call finalize() from Object class
	}
}
