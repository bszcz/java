// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

package webapp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebApp extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<!doctype html>");
		out.println("<html lang=en>");
		out.println("<head>");
		out.println("\t<meta http-equiv=refresh content=1>");
		out.println("</head>");
		out.println("<body>");
		out.println("\t<h1>Yes, this is servlet.</h1>");
		out.println("\t<pre>");
		out.println(runCommand("uptime"));
		out.println("\t</pre>");
		out.println("</body>");
		out.println("</html>");
		out.close();
	}

	private String runCommand(final String command) {
		String output = "";
		try {
			Process proc = Runtime.getRuntime().exec(command);
			InputStreamReader isr = new InputStreamReader(proc.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			String line = "";
			while ((line = br.readLine()) != null) {
				output += line + "\n";
			}
			proc.waitFor();
			proc.destroy();
		} catch (Exception e) {
			// ignore exceptions
		}
		return output;
	}
}
