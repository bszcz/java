// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

class Equility {
	public static void main(String[] args) {
		Player dolan = new Player("Dolan", 100);
		Player gooby = new Player("Gooby", 100);

		Player pruto = dolan; // actually is Dolan

		System.out.println("(dolan == pruto) = " + (dolan == pruto));
		System.out.println("(dolan == gooby) = " + (dolan == gooby));
		System.out.println("dolan.equal(gooby) = " + dolan.equal(gooby));
	}
}

class Player {
	public String name;
	public int points;

	public Player(String name, int points) {
		this.name = name;
		this.points = points;
	}

	// Defining 'equal' instead of overriding 'equals'.
	// Overriding 'equals' requires overriding 'hashCode'.
	public boolean equal(Player that) {
		return this.points == that.points;
	}
}
