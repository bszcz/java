// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.util.Arrays;

class Comparing {
	private static final String[] cmpWords = {"older than", "same age as", "younger than"};

	public static void main(String[] args) {
		Lang c = new Lang("C", 1972);
		Lang cpp = new Lang("C++", 1983);
		Lang objc = new Lang("Objective-C", 1983);
		Lang java = new Lang("Java", 1995);
		Lang csharp = new Lang("C#", 2000);

		cmpAge(c, cpp);
		cmpAge(cpp, objc);
		cmpAge(csharp, java);

		System.out.println("--");

		Lang[] langs = {c, cpp, csharp, java, objc};
		printLangs(langs);

		System.out.println("--");

		Arrays.sort(langs);
		printLangs(langs);
	}

	public static void cmpAge(Lang lang1, Lang lang2) {
		int index = lang1.compareTo(lang2) + 1;
		System.out.println(lang1 + " is " + cmpWords[index] + " " + lang2 + ".");
	}

	public static void printLangs(Lang[] langs) {
		for (int i = 0; i < langs.length; i++) {
			System.out.println("lang[" + i + "] = " + langs[i]);
		}
	}
}

class Lang implements Comparable<Lang> {
	public String name;
	public int year;

	public Lang(String name, int year) {
		this.name = name;
		this.year = year;
	}

	public String toString() {
		return name + "(" + year + ")";
	}

	@Override
	public int compareTo(Lang lang) {
		if (this.year < lang.year) {
			return -1;
		} else if (this.year == lang.year) {
			return 0;
		} else {
			return +1;
		}
	}
}
