// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.util.Random;

class Sync {
	public static void main(String[] args) {
		FiniteStack fstack = new FiniteStack();
		Popper popper = new Popper(fstack);
		Pusher pusher = new Pusher(fstack, popper);

		popper.start();
		pusher.start();

		System.out.println("Pusher and Popper started.");
	}
}

class FiniteStack {
	private final int STACK_SIZE = 8;
	private int[] fstack;
	private int index;

	public FiniteStack() {
		fstack = new int[STACK_SIZE];
		index = 0;
	}

	public boolean isEmpty() {
		if (index == 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isFull() {
		if (index == STACK_SIZE) {
			return true;
		} else {
			return false;
		}
	}

	public void push(int num) {
		if (!isFull()) {
			fstack[index] = num;
			index++;
		}
	}

	public int pop() {
		if (!isEmpty()) {
			index--;
			return fstack[index];
		} else {
			return 0;
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("FiniteStack{");
		for (int i = 0; i < index; i++) {
			sb.append(fstack[i]);
			if (i + 1 < index) {
				sb.append(", ");
			}
		}
		sb.append("}");
		return sb.toString();
	}
}

class Pusher extends Thread {
	private final int RAND_RANGE = 100;
	private Random randGen;
	private FiniteStack fstack;
	private Popper popper;

	public Pusher(FiniteStack fstack, Popper popper) {
		randGen = new Random(12345);
		this.fstack = fstack;
		this.popper = popper;
	}

	public void run() {
		while (popper.isAlive()) {
			synchronized (fstack) {
				if (!fstack.isFull()) {
					fstack.push(randGen.nextInt(RAND_RANGE));
				}
			}
		}
		System.out.println("Returning from Pusher.");
		return;
	}

}

class Popper extends Thread {
	private final int NUM_POPS = 10;
	private FiniteStack fstack;

	public Popper(FiniteStack fstack) {
		this.fstack = fstack;
	}

	public void run() {
		int pops_todo = NUM_POPS;
		while (pops_todo > 0) {
			synchronized (fstack) {
				if (!fstack.isEmpty()) {
					String j = String.format("%2d", pops_todo);
					System.out.println(j + ":");
					System.out.println("\tfstack = " + fstack);
					System.out.println("\tfstack.pop() = " + fstack.pop());
					pops_todo--;
				}
			}
		}
		System.out.println("Returning from Popper.");
		return;
	}
}
