// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.awt.*;
import javax.swing.*;

public class HelloApplet extends JApplet {
	public void init() {
		Container content = getContentPane();
		content.setLayout(new FlowLayout());
		JLabel label = new JLabel("Hello Applet !!!");
		content.add(label);
	}
}
