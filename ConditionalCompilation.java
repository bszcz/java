// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

class ConditionalCompilation {
	static final long LOOPS = 1_000_000_000L; // thousand separator (Java 7+)

	static boolean debug1 = false;
	static final boolean debug2 = false;

	public static void main(String[] args) {
		long t = 0;

		t = System.nanoTime();
		func1();
		t = System.nanoTime() - t;
		System.out.println("func1: " + nanosecToSec(t) + " seconds");

		t = System.nanoTime();
		func2();
		t = System.nanoTime() - t;
		System.out.println("func2: " + nanosecToSec(t) + " seconds");

		t = System.nanoTime();
		func3();
		t = System.nanoTime() - t;
		System.out.println("func3: " + nanosecToSec(t) + " seconds");

		t = System.nanoTime();
		func4();
		t = System.nanoTime() - t;
		System.out.println("func4: " + nanosecToSec(t) + " seconds");
	}

	public static double nanosecToSec(long t) {
		final double NANOSEC_IN_SEC = 1_000_000_000.0;
		return (double)t / NANOSEC_IN_SEC;
	}

	public static boolean returnFalse(final int num) {
		System.out.println("func" + num + ": returning false...");
		return false;
	}

	public static void func1() {
		for (long i = 0; i < LOOPS; i++) {
			if (debug1) {
				System.out.println("func1: debugging...");
			}
		}
	}

	public static void func2() {
		for (long i = 0; i < LOOPS; i++) {
			if (debug2) {
				System.out.println("func2: debugging...");
			}
		}
	}


	public static void func3() {
		for (long i = 0; i < LOOPS; i++) {
			if (debug1 && returnFalse(3)) {
				System.out.println("func3: debugging...");
			}
		}
	}

	public static void func4() {
		for (long i = 0; i < LOOPS; i++) {
			if (debug2 && returnFalse(4)) {
				System.out.println("func4: debugging...");
			}
		}
	}
}
