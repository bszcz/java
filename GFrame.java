// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.awt.*;
import javax.swing.*;

class GFrame extends JFrame {
	public GFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("9999 in 1");
		setSize(256, 512);
		Container content = this.getContentPane();
		content.setBackground(Color.white);
		GCanvas canvas = new GCanvas();
		content.add(canvas);
		setVisible(true);
	}

	public static void main(String arg[]) {
		new GFrame();
	}
}

class GCanvas extends Canvas {
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		for (int i = 0; i < 100; i++) {
			int x = (int)(Math.random() * 256.0 / 4.0) * 4;
			int y = (int)(Math.random() * 512.0 / 4.0) * 4;
			g2d.fillRect(x, y, 4, 4);
		}
	}
}
