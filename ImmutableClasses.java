// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.lang.reflect.Field;

class ImmutableClasses {
	public static void main(String[] arg) {
		Point point = new Point(1);
		Immutable1 immut1 = new Immutable1(1234, point);
		Immutable2 immut2 = new Immutable2(1234, point);

		System.out.println("immut1.getValue() = " + immut1.getValue());
		System.out.println("immut2.getValue() = " + immut2.getValue());
		try {
			Field value1 = immut1.getClass().getDeclaredField("value");
			Field value2 = immut2.getClass().getDeclaredField("value");
			value1.setAccessible(true);
			value2.setAccessible(true);
			value1.set(immut1, 5678);
			value2.set(immut2, 5678); // works dispite 'final' modifier
			System.out.println("immut1.getValue() = " + immut1.getValue());
			System.out.println("immut2.getValue() = " + immut2.getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("immut1.getPointX() = " + immut1.getPointX());
		System.out.println("immut2.getPointX() = " + immut2.getPointX());
		point.x = 2;
		System.out.println("immut1.getPointX() = " + immut1.getPointX());
		System.out.println("immut2.getPointX() = " + immut2.getPointX());
		Point point1 = immut1.getPoint();
		Point point2 = immut2.getPoint();
		point1.x = 3;
		point2.x = 3;
		System.out.println("immut1.getPointX() = " + immut1.getPointX());
		System.out.println("immut2.getPointX() = " + immut2.getPointX());
	}
}

class Immutable1 {
	private int value;
	private final Point point;

	public Immutable1(int value, Point point) {
		this.value = value;
		this.point = point;
	}

	public int getValue() {
		return value;
	}

	public int getPointX() {
		return point.x;
	}

	public Point getPoint() {
		return point;
	}
}

class Immutable2 {
	private final int value; // still mutable despite 'final' modifier
	private final Point point;

	public Immutable2(int value, Point point) {
		this.value = value;
		this.point = new Point(point.x);
	}

	public int getValue() {
		return value;
	}

	public int getPointX() {
		return point.x;
	}

	public Point getPoint() {
		return new Point(point.x);
	}
}

class Point {
	public int x = 0;

	public Point(int x) {
		this.x = x;
	}
}
