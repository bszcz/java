// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.util.LinkedList;
import java.util.Random;

class RandomMeans {
	public static void main(String[] args) {
		final int maxSize = 128;
		RQueue rq = new RQueue(maxSize);

		final int maxRand = 1000;
		final int numGens = 4;
		RGenerator[] rgs = new RGenerator[numGens];
		for (int i = 0; i < numGens; i++) {
			rgs[i] = new RGenerator(rq, maxRand);
			rgs[i].start();
		}

		RAccumulator ra = new RAccumulator(rq);
		ra.run();
	}
}

class RQueue {
	private LinkedList<Integer> rqueue = new LinkedList<Integer>();
	private final int maxSize;

	public RQueue(final int maxSize) {
		this.maxSize = maxSize;
	}

	public synchronized void put(int rand) {
		while (rqueue.size() >= maxSize) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		rqueue.add(rand);
		notifyAll();
	}

	public synchronized int get() {
		while (rqueue.size() <= 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		int rand = rqueue.remove();
		notifyAll();
		return rand;
	}
}

class RAccumulator {
	private final RQueue rqueue;

	public RAccumulator(final RQueue rqueue) {
		this.rqueue = rqueue;
	}

	public void run() {
		final long dt = 1_000_000_000; // nanoseconds
		long t = System.nanoTime();
		long count = 0;
		long acc = 0;
		for (;;) {
			if (System.nanoTime() - t > dt) {
				double mean = (double)acc / (double)count;
				System.out.printf(
					"mean = %.3f (acc = %d, count = %d)\n",
					mean, acc, count
				);
				t += dt;
			}
			acc += rqueue.get();
			count++;
		}
	}
}

class RGenerator extends Thread {
	private final RQueue rqueue;
	private final int maxRand; // generates [0, maxRand]

	public RGenerator(final RQueue rqueue, final int maxRand) {
		this.rqueue = rqueue;
		this.maxRand = maxRand + 1; // since nextInt(n) generates [0, n)
	}

	public void run() {
		Random rgen = new Random();
		for (;;) {
			rqueue.put(rgen.nextInt(maxRand));
		}
	}
}
