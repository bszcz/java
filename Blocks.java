// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

class Blocks {
	public static void main(String[] args) {
		Foo f1 = new Foo();
		Foo f2 = new Foo();
		Foo f3 = new Foo();

		System.out.println(f1);
		System.out.println(f2);
		System.out.println(f3);
	}
}

class Foo {
	public static final long staticTime;
	public final long time;

	static {
		System.out.println("static{}");
		staticTime = System.nanoTime();
	}

	{
		System.out.println("{}");
		time = System.nanoTime();
	}

	public Foo() {
	}

	public String toString() {
		return "{staticTime:" + staticTime + ", time:" + time + "}";
	}
}
