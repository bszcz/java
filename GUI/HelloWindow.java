// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;

public class HelloWindow {
	public static void main(String[] args) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = screenSize.width;
		int height = screenSize.height;
		JFrame frame = new HelloFrame(
			width / 3,
			4 * height / 5,
			width / 3,
			height / 10
		);
		frame.show();
	}
}

class HelloFrame extends JFrame {
	private String[] greenTexts = {
		"be 26",
		"like coding",
		"start learning Java",
		"make window",
		"add title",
		"set position",
		"set size",
		"add pane",
		"add text",
		"set font",
		"set color",
		"what am i doing with my life"
	};

	HelloFrame(int width, int height, int xLoc, int yLoc) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Hello Window");
		setLocation(xLoc, yLoc);
		setSize(width, height);

		JPanel panel = new JPanel();
		//panel.setLayout(new GridLayout(greenTexts.length, 1));
		for (int i = 0; i < greenTexts.length; i++) {
			panel.add(new HelloPanel(greenTexts[i]));
		}
		getContentPane().add(panel);
	}
}

class HelloPanel extends JPanel {
	Border border = BorderFactory.createLineBorder(Color.lightGray);

	HelloPanel(String greenText) {
		setBackground(Color.pink);
		setBorder(border);
		JLabel label = new JLabel("> " + greenText);
		label.setFont(new Font("Helvetica", Font.BOLD | Font.ITALIC, 18));
		label.setForeground(new Color(0, 127, 0));
		add(label);
	}
}
