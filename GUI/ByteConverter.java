// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ByteConverter {
	public static void main(String[] args) {
		JFrame frame = new ConverterFrame();
		frame.show();
	}
}

class ConverterFrame extends JFrame {
	ConverterFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Byte Converter");
		setSize(600, 300);
		setLocation(100, 100);
		getContentPane().add(new ConverterPanel());
	}
}

class ConverterUnits extends Choice {
	String[] units = {"B", "KB", "MB", "GB", "TB"};

	ConverterUnits() {
		for (int i = 0; i < units.length; i++) {
			addItem(units[i]);
		}
	}
}

class ConverterPanel extends JPanel implements ActionListener {
	JTextField inputSize;
	Choice units;
	JButton run;
	JLabel outputSizes;

	ConverterPanel() {
		JPanel topPanel = new JPanel();
		inputSize = new JTextField(10);
		units = new ConverterUnits();
		run = new JButton("Convert");
		run.addActionListener(this);
		topPanel.add(inputSize);
		topPanel.add(units);
		topPanel.add(run);

		JPanel bottomPanel = new JPanel();
		outputSizes = new JLabel("No Input Given");
		bottomPanel.add(outputSizes);

		setLayout(new GridLayout(2, 1));
		add(topPanel);
		add(bottomPanel);
	}

	public void actionPerformed(ActionEvent event) {
		String str = "";
		try {
			double size = Double.parseDouble(inputSize.getText());
			size = size * Math.pow(10, 3 * units.getSelectedIndex());
			str = convertSize(size);
		} catch (Exception e) {
			str = "Error: " + e;
		}
		outputSizes.setText(str);
	}

	String convertSize(double size) {
		String str = "";
		str += "<html>";
		str += String.format("%.0f", size) + " B<br>";
		str += String.format("%.3f", size / 1024) + " KiB<br>";
		str += String.format("%.3f", size / 1024 / 1024) + " MiB<br>";
		str += String.format("%.3f", size / 1024 / 1024 / 1024) + " GiB<br>";
		str += String.format("%.3f", size / 1024 / 1024 / 1024 / 1024) + " TiB<br>";
		str += "</html>";
		return str;
	}
}
