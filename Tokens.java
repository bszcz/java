// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.io.*;

class Tokens {
	public static void main(String[] args) {
		final String filename = "Tokens.java";

		try {
			FileInputStream fis = new FileInputStream(filename);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			StreamTokenizer st = new StreamTokenizer(br);
			st.eolIsSignificant(true);
			st.slashStarComments(true);
			st.slashSlashComments(true);
			printTokens(st);
			br.close(); // closes underlying streams
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void printTokens(StreamTokenizer st) throws IOException {
		while (st.nextToken() != StreamTokenizer.TT_EOF) {
			switch (st.ttype) {
			case StreamTokenizer.TT_NUMBER:
				System.out.print(st.nval + "@NUMBER ");
				break;
			case StreamTokenizer.TT_WORD:
				System.out.print(st.sval + "@WORD ");
				break;
			case StreamTokenizer.TT_EOL:
				System.out.println("@EOL ");
				break;
			case '"':
			case '\'':
				System.out.print(st.sval + "@QUOTED ");
				break;
			default:
				System.out.print((char)st.ttype + "@OTHER ");
			}
		}
	}
}
