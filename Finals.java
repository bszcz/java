// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

class Finals {
	public static void main(String[] args) {
		final int neverInitialised; // this is fine

		final int i;
		i = 0;
		//i = 1; // error
		//i++; // error

		final Examples exs = new Examples(0);
		//exs = new Examples(1); // error: cannot assign a value to final variable exs
		System.out.println("exs.variable = " + exs.variable);
		changeVariable(exs);
		System.out.println("exs.variable = " + exs.variable);

		System.out.println("The word 'const' is a reserved word but it is not used.");
	}

	public static void changeVariable(final Examples exs) {
		//exs = new Examples(1); // error: final parameter exs may not be assigned
		exs.variable = 1; // but this works
	}
}

final class Examples {
	private final int constant;
	public int variable;

	public Examples(final int c) {
		//c = 0; // error: final parameter c may not be assigned
		this.constant = c; // initialise here or get an error
		this.variable = c;
	}
}

//class MoreExamples extends Examples {} // error: cannot inherit from final class Examples

class FinalMethods {
	public FinalMethods() {}
	public final void m() {}
}

class OverrideMethods extends FinalMethods {
	//public void m() {} // error: m() in OverrideMethods cannot override m() in FinalMethods
}
