// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

public class Exceptions {
	private static final int SIZE = 3;

	public static void main(String[] args) {
		String[] strArray = {"0.123", "hello"};

		System.out.println("\nparse: ");
		parse(strArray);

		System.out.println("\nparseCheck: ");
		parseCheck(strArray);

		System.out.println("\nparseThrow: ");
		try {
			parseThrow(strArray);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("detected exception: " + e);
		}
	}

	private static void parse(String[] strArray) {
		double num;
		for (int s = 0; s < SIZE; s++) {
			num = 0.0;
			try {
				num = Float.parseFloat(strArray[s]);
			} catch (NumberFormatException e) {
				System.out.println("detected exception: NumberFormatException");
			} catch (Exception e) {
				System.out.println("detected other exception: " + e);
			} finally {
				System.out.println("parsed: " + num);
			}
		}
	}

	private static void parseCheck(String[] strArray) {
		try {
			if (strArray.length < SIZE) {
				throw new ArrayIndexOutOfBoundsException("strArray must have " + SIZE + " items!");
			} else {
				parse(strArray);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("detected exception: " + e);
		}
	}

	private static void parseThrow(String[] strArray) throws ArrayIndexOutOfBoundsException {
		String dummy = strArray[SIZE - 1];
		parse(strArray);
	}
}
