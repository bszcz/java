// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.eu>
// This program is free software under the MIT license.

class AccessGranted {
	public static void main(String[] args) {
		Digit dig1 = new Digit(-137);
		System.out.println("dig1 = " + dig1);

		Digit dig2 = new Digit(0);
		dig2.hack(dig1, 10);
		System.out.println("dig1 = " + dig1);

		DigitExt dig3 = new DigitExt(0);
		dig3.hackExt(dig1, 100);
		System.out.println("dig1 = " + dig1);
	}
}

class Digit {
	protected int value;

	public Digit(int d) {
		setValue(d);
	}

	// guard against non-digits
	private void setValue(int d) {
		if (0 <= d && d <= 9) {
			value = d;
		}
	}

	public String toString() {
		return Integer.toString(value);
	}

	public void hack(Digit d, int value) {
		d.value = value;
	}
}

class DigitExt extends Digit {
	public DigitExt(int d) {
		super(d);
	}

	public void hackExt(Digit d, int value) {
		d.value = value;
	}
}
