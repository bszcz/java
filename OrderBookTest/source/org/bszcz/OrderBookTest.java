package org.bszcz;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class OrderBookTest {
	static List<Integer> levels;
	static Random rand;

	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("List type and level depth not provided.");
			System.exit(1);
		}

		final int seed = 12345;
		rand = new Random(seed);

		final String listType = setListType(args[0]);
		System.out.println("listType = " + listType);

		final int maxLevel = Integer.valueOf(args[1]);
		System.out.println("maxLevel = " + maxLevel);

		final long startTime = System.nanoTime();
		run(100, maxLevel);
		final double testTime = System.nanoTime() - startTime;
		System.out.printf("testTime = %.3f seconds\n", testTime / 1e9);
	}

	static String setListType(String type) {
		if (type.equals("ArrayList")) {
			levels = new ArrayList<Integer>();
			return "ArrayList";
		} else {
			levels = new LinkedList<Integer>();
			return "LinkedList";
		}
	}

	static void run(final int runs, final int runSize) {
		for (int i = 0; i < runs; i++) {
			Integer level;
			for (int j = 0; j < runSize; j++) {
				level = new Integer(j);
				addLevel(level);
			}
			for (int j = 0; j < runSize; j++) {
				level = new Integer(j);
				removeLevel(level);
			}
		}
	}

	static void addLevel(Integer newLevel) {
		final int size = levels.size();
		if (size == 0) {
			levels.add(newLevel);
			return;
		}

		final int index = levels.indexOf(newLevel);
		if (index == -1) {
			for (int i = 0; i < size; i++) {
				if (levels.get(i).compareTo(newLevel) == -1) {
					levels.add(i, newLevel);
					return;
				}
			}
			levels.add(newLevel);
		}
	}

	static void removeLevel(Integer oldLevel) {
		levels.remove(oldLevel);
	}
}
