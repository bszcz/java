// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

class VariableArguments {
	public static void main(String[] args) {
		int sum = 0;

		sum = add(2, 2);
		System.out.println("sum = " + sum);

		sum = add(1, 2, 3, 4, 5, 6, 7, 8, 9);
		System.out.println("sum = " + sum);
	}

	public static int add(int... nums) {
		int sum = 0;
		for (int n : nums) {
			sum += n;
		}
		return sum;
	}
}
