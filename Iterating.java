// Copyright (c) 2014 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.util.Iterator;
import java.util.NoSuchElementException;

class Iterating {
	public static void main(String[] args) {
		String str = "zaq12wsx cde34rfv bgt56yhn mju78ik,";
		System.out.println("str = \"" + str + "\"");

		OnlyDigitsIter odi;

		//using Iterator interface
		odi = new OnlyDigitsIter(str);
		while (odi.hasNext()) {
			System.out.println("odi.next() = " + odi.next());
		}

		// with numbering
		odi = new OnlyDigitsIter(str); // can't 'rewind', need a new one
		for (int i = 0; odi.hasNext(); i++) {
			System.out.println(i + ": odi.next() = " + odi.next());
		}

		// using Iterable interface
		odi = new OnlyDigitsIter(str);
		for (char d : odi) {
			System.out.println("d = " + d);
		}
	}
}

class OnlyDigitsIter implements Iterator<Character>, Iterable<Character> {
	private final int NO_NEXT = -1;
	private int nextIndex;
	private String str;

	public OnlyDigitsIter(String str) {
		this.str = new String(str);
		nextIndex = NO_NEXT;
		findNext();
	}

	private void findNext() {
		// when called first time, nextIndex = NO_NEXT = -1
		// hence, the string is searched from the beginning
		for (int i = nextIndex + 1; i < str.length(); i++) {
			if (Character.isDigit(str.charAt(i))) {
				nextIndex = i;
				return;
			}
		}
		nextIndex = NO_NEXT;
	}

	// Iterator interface: hasNext(), next(), remove()

	public boolean hasNext() {
		if (nextIndex == NO_NEXT) {
			return false;
		} else {
			return true;
		}
	}

	public Character next() throws NoSuchElementException {
		if (hasNext()) {
			char nextDigit = str.charAt(nextIndex);
			findNext();
			return nextDigit;
		} else {
			throw new NoSuchElementException();
		}
	}

	public void remove() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	// Iterable interface: iterator()

	public Iterator<Character> iterator() {
		return this;
	}
}
