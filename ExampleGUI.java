// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ExampleGUI {
	private static final String title = "Hello from Java GUI";
	private static final int width = 640;
	private static final int height = 480;

	public static void main(String[] args) {
		SwingWindow window = new SwingWindow(title, width, height);
	}

}

class SwingWindow extends JFrame {
	public SwingWindow(String title, int width, int height) {
		super(title);
		setSize(width, height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		Container content = getContentPane();
		content.setBackground(Color.white);
		GridBagLayout flow = new GridBagLayout();
		GridBagConstraints pos = new GridBagConstraints();
		content.setLayout(flow);

		JPanel panel = new JPanel();
		JButton startButton = new JButton("Start");
		JButton stopButton = new JButton("Stop");
		panel.setBackground(Color.black);
		panel.add(startButton);
		panel.add(stopButton);
		pos.gridx = 0;
		pos.gridy = 0;
		content.add(panel, pos);

		JLabel label = new JLabel("This is some label.");
		JTextField textField = new JTextField("This is some text field.");
		JTextArea textArea = new JTextArea("This is some text area.", 5, 10);
		JScrollPane scroll = new JScrollPane(
			textArea,
			JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
		);
		pos.gridx = 1;
		pos.gridy = 5;
		content.add(label, pos);
		pos.gridx = 1;
		pos.gridy = 10;
		content.add(textField, pos);
		pos.gridx = 1;
		pos.gridy = 15;
		content.add(textArea, pos);
		content.add(scroll, pos);

		ButtonGroup radios = new ButtonGroup();
		JRadioButton radio1 = new JRadioButton("Yes");
		JRadioButton radio2 = new JRadioButton("No");
		JCheckBox check1 = new JCheckBox("I agree");
		radios.add(radio1);
		radios.add(radio2);
		pos.gridx = 5;
		pos.gridy = 5;
		content.add(radio1, pos);
		pos.gridx = 5;
		pos.gridy = 10;
		content.add(radio2, pos);
		pos.gridx = 5;
		pos.gridy = 15;
		content.add(check1, pos);

		JComboBox<String> langs = new JComboBox<String>();
		langs.addItem("English");
		langs.addItem("French");
		langs.addItem("German");
		pos.gridx = 10;
		pos.gridy = 20;
		content.add(langs, pos);

		JSlider slider = new JSlider(0, 10);
		slider.setMajorTickSpacing(5);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		pos.gridx = 0;
		pos.gridy = 30;
		content.add(slider, pos);

		setContentPane(content);
	}
}
